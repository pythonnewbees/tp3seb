from othello.piece import Piece


class Planche:
    """
    Classe représentant la planche d'un jeu d'Othello.
    """
    directions = [[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1]]

    def __init__(self):
        """
        Méthode spéciale initialisant une nouvelle planche.
        """
        # Dictionnaire de cases. La clé est une position (ligne, colonne), et la valeur une instance de la classe Piece.
        self.cases = {}

        # Appel de la méthode qui initialise une planche par défaut.
        self.initialiser_planche_par_default()

        # On joue au Othello 8x8
        self.nb_cases = 8

    def get_piece(self, position):
        """
        Récupère une pièce dans la planche.

        Args:
            position: La position où récupérer la pièce, un tuple de coordonnées matricielles (ligne, colonne).

        Returns:
            La pièce à cette position s'il y en a une, None autrement.
        """
        if position in self.cases.keys():
            return self.cases[position]
        else:
            return None

    def position_valide(self, position):
        """
        Vérifie si une position est valide (chaque coordonnée doit être dans les bornes).

        Args:
            position: Un couple (ligne, colonne), tuple de deux éléments.

        Returns:
            True si la position est valide, False autrement
        """
        if position[0] in range(0,8) and position[1] in range(0,8):
            return True
        else:
            return False

    def obtenir_positions_mangees(self, position, couleur):
        """
        Détermine quelles positions seront mangées si un coup de la couleur passée est joué à la position passée.

        ***RETOURNEZ SEULEMENT LA LISTE DES POSITIONS MANGÉES, NE FAITES PAS APPEL À piece.echange_couleur() ICI.***

        Ici, commencez par considérer que, pour la position évaluée, des pièces peuvent être mangées dans 8 directions
        distinctes représentant les 8 cases qui entourent la position évaluée. Vous devez donc vérifier, pour chacune
        de ces directions, combien de pièces sont mangées et retourner une liste regroupant les pièces mangées dans
        toutes les directions.

        Ici, une direction représente une liste de 2 entiers pour le déplacement en x et y. Par exemple, pour la
        direction diagonale où en explore vers le bas et vers la droite, on utiliserait la liste [1, 1]. De même, pour
        la direction gauche, on utiliserait la liste [-1, 0]. Il y a donc un total de 8 directions, représentant les
        8 positions auxquelles la position actuelle peut toucher.

        Pensez à faire appel à la fonction obtenir_positions_mangees_direction().

        Args:
            position: La position du coup à jouer.
            couleur: La couleur du coup à jouer.

        Returns:
            une liste contenant toutes les positions qui seraient mangées par le coup.
        """
        positions_mangees = []
        for direction in Planche.directions:
            positions_mangees += self.obtenir_positions_mangees_direction(couleur, direction, position)
        return positions_mangees

    def obtenir_positions_mangees_direction(self, couleur, direction, position):
        """
        Détermine les positions qui seront mangées si un coup de couleur "couleur" est joué à la position "position",
        si on parcourt la planche dans une direction "direction".

        Pour une direction donnée, vous devez parcourir la planche de jeu dans la direction. Tant que votre déplacement
        vous mène sur une pièce de la couleur mangée, vous continuez de vous déplacer. Trois situations surviennent
        alors pour mettre un terme au parours dans la direction :

        1) Vous arrivez sur une case vide. Dans ce cas, aucune pièce n'est mangée.

        2) Vous arrivez sur une case extérieure à la planche. Encore une fois, aucune pièce n'est mangée.

        3) Vous arrivez sur une case de la même couleur que le coup initial. Toutes les pièces de la couleur opposée
        que vous avez alors rencontrées dans votre parcours doivent alors être ajoutées à grande liste des pièces
        mangées cumulées.

        N.B. : Cette méthode peut être implémentée par au moins deux techniques différentes alors laissez place à votre
        imagination et explorez ! Une méthode faisant une boucle d'exploration complète et une méthode de parcours
        récursif  sont quelques-unes des façons de faire que vous pouvez explorer. Il se peut même que votre solution
        ne soit pas dans les solutions énumérées précédemment.

        Args:
            couleur: La couleur du coup évalué
            direction: La direction de parcours évaluée
            position: La position du coup évalué

        Returns:
            La liste (peut-être vide) de toutes les positions mangées à partir du coup et de la direction donnés.
        """
        positions_mangees = []
        new_position = position[0]+ direction[0],  position[1]+ direction[1]
        position_en_jeu = True
        while position_en_jeu:
            if self.position_valide(new_position):
                if self.get_piece(new_position) != None :
                    if self.get_piece(new_position).couleur != couleur:
                        positions_mangees.append(new_position)
                        new_position = new_position[0] + direction[0], new_position[1] + direction[1]
                    else:
                        break  # Garder la liste, on a un vecteur de pieces a tourner
                else:
                    # Vider la liste, elle n'est pas contenue entre 2 pieces de la meme couleur
                    positions_mangees.clear()
                    position_en_jeu = False
            else:
                # Vider la liste, elle n'est pas contenue entre 2 pieces de la meme couleur
                positions_mangees.clear()
                position_en_jeu = False
        return positions_mangees

    def coup_est_possible(self, position, couleur):
        """
        Détermine si un coup est possible. Un coup est possible si au moins une pièce est mangée par celui-ci.

        Args:
            position: La position du coup évalué
            couleur: La couleur du coup évalué

        Returns:
            True, si le coup est valide, False sinon
        """
        positions_mangees = self.obtenir_positions_mangees(position, couleur)
        if len(positions_mangees) > 0:
            return True
        else:
            return False

    def lister_coups_possibles_de_couleur(self, couleur):
        """
        Fonction retournant la liste des coups possibles d'une certaine couleur. Un coup est considéré possible
        si au moins une pièce est mangée quand la couleur "couleur" joue à une certaine position, ne l'oubliez pas!

        ATTENTION: ne dupliquez pas de code déjà écrit! Réutilisez les fonctions déjà programmées!

        Args:
            couleur: La couleur ("blanc", "noir") des pièces dont on considère le déplacement, un string

        Returns:
            Une liste de positions de coups possibles pour la couleur "couleur"
        """
        coups_possibles = []
        for i in range(8):
            for j in range(8):
                if self.get_piece((i,j)) == None:
                    coup_pour_case = self.obtenir_positions_mangees((i,j), couleur)
                    if len(coup_pour_case) > 0:
                        coups_possibles.append((i,j))

        return coups_possibles

    def jouer_coup(self, position, couleur):
        """
        Joue une pièce de la couleur "couleur" à la position "position".

        Cette méthode doit également:
        - Ajouter la pièce aux pièces de la planche.
        - Faire les changements de couleur pour les pièces mangées par le coup.
        - Retourner un message indiquant "ok" ou "erreur".

        ATTENTION: Ne dupliquez pas de code! Vous savez déjà qu'un coup est valide si au moins une pièce est mangée
                   par celui-ci. Vous avez une méthode qui fait exactement ce travail à programmer !

        Args:
            position: La position du coup.
            couleur: La couleur du coup.

        Returns:
            "ok" si le déplacement a été effectué car il est valide, "erreur" autrement.
        """
        if self.coup_est_possible(position, couleur):
            pieces_mangees = self.obtenir_positions_mangees(position, couleur)
            self.cases[position] = Piece(couleur)
            # On change toutes les pieces retournées de couleur
            for position in pieces_mangees:
                piece = self.get_piece(position)
                piece.echange_couleur()
            return "ok"
        else:
            return "erreur"

    def convertir_en_chaine(self):
        """
        Retourne une chaîne de caractères où chaque case est écrite sur une ligne distincte.
        Chaque ligne contient l'information suivante :
        ligne,colonne,couleur

        Cette méthode pourrait par la suite être réutilisée pour sauvegarder une planche dans un fichier.

        Returns:
            La chaîne de caractères.
        """
        chaine = ""
        for case in self.cases:
            chaine += ','.join([str(case[0]), str(case[1]), self.cases[case].couleur]) + "\n"
        return chaine

    def charger_dune_chaine(self, chaine):
        """
        Remplit la planche à partir d'une chaîne de caractères comportant l'information d'une pièce sur chaque ligne.
        Chaque ligne contient l'information suivante :
        ligne,colonne,couleur

        Args:
            chaine (str): La chaîne de caractères, un string.
        """
        self.cases.clear()
        lignes = chaine.split("\n")
        for case in lignes:
            if len(case) > 0: # On saute les lignes vides
                x, y, couleur = case.split(',')
                # Pour ne pas insérer des pièces a des positions invalides de la planche
                if self.position_valide((int(x), int(y))):
                    self.cases[(int(x), int(y))] = Piece(couleur)
        return

    def initialiser_planche_par_default(self):
        """
        Initialise une planche de base avec la position initiale des pièces.
        """
        self.cases.clear()
        self.cases[(3, 3)] = Piece("blanc")
        self.cases[(3, 4)] = Piece("noir")
        self.cases[(4, 3)] = Piece("noir")
        self.cases[(4, 4)] = Piece("blanc")

    def __repr__(self):
        """
        Cette méthode spéciale permet de modifier le comportement d'une instance de la classe Planche pour l'affichage.
        Faire un print(une_planche) affichera la planche à l'écran.
        """
        s = "  +-0-+-1-+-2-+-3-+-4-+-5-+-6-+-7-+\n"
        for i in range(0, self.nb_cases):
            s += str(i)+" | "
            for j in range(0, self.nb_cases):
                if (i, j) in self.cases:
                    s += str(self.cases[(i, j)])+" | "
                else:
                    s += "  | "
            s += str(i)
            if i != self.nb_cases - 1:
                s += "\n  +---+---+---+---+---+---+---+---+\n"

        s += "\n  +-0-+-1-+-2-+-3-+-4-+-5-+-6-+-7-+\n"

        return s

    @property
    def est_pleine(self):
        """
        Compte le nombre de cases encore vides dans la planche pour savoir si la planche de jeu est pleine.
        Sera utile pour déterminer si la partie est terminée. Bien que l'on pourrait faire ceci directement en
        accédant aux cases de la planche, cela contrevient au principe d'encapsulation

        Returns:
            (bool): True si la planche est pleine (64), False sinon
        """
        return len(self.cases.keys()) == 64

    def compter_pieces(self):
        """
        Compte le nombre de pièces de chaque couleur sur la planche

        Returns:
            (tuple): Nombre de pièces noir, Nombre de pièces blanche
        """
        noir = 0
        blanc = 0
        for case in self.cases.keys():
            if self.cases[case].est_noir():
                noir += 1
            else:
                blanc += 1
        return noir, blanc

if __name__ == '__main__':
    ma_planche = Planche()
    assert ma_planche.get_piece((1,1)) == None
    assert ma_planche.position_valide((1,1))
    assert not ma_planche.position_valide((1,8))
    assert not ma_planche.position_valide((8, 7))
    assert ma_planche.position_valide((0, 4))

    #Test de chargement de planche
    pl = "1,1,noir\n1,2,blanc\n1,3,blanc\n1,4,blanc\n4,3,noir\n5,4,noir\n4,4,blanc\n5,3,blanc\n"
    ma_planche.charger_dune_chaine(pl)
    print(ma_planche)
    assert ma_planche.obtenir_positions_mangees((1, 5), "noir") == [(1, 4), (1, 3), (1, 2)]
    assert ma_planche.obtenir_positions_mangees((1, 5), "blanc") == []
    assert (ma_planche.convertir_en_chaine()) == pl
    assert ma_planche.lister_coups_possibles_de_couleur("noir") == [(1, 5), (3, 4), (4, 5), (5, 2), (6, 3)]
    assert ma_planche.lister_coups_possibles_de_couleur("blanc") == [(1, 0), (3, 3), (4, 2), (5, 5), (6, 4)]