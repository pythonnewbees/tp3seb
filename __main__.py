"""
Module principal du package othello. C'est ce module que nous allons exécuter pour démarrer votre jeu.
"""

from othello.partie import Partie
import datetime

if __name__ == '__main__':
    # Création d'une instance de Partie.
    partie = Partie()

    # Si on veut charger une partie à partir d'une partie sauvegardée.
    #partie = Partie("C:\\Users\\ Sebastien.Gadbois\\PycharmProjects\\tp3seb\\2018-11-10-21_02_02_Othello.txt")

    # Démarrage de cette partie.
    partie.jouer()
    # Pour sauvegarder la partie dans un fichier
    now = datetime.datetime.now()
    filename = now.strftime("%Y-%m-%d-%H_%M_%S") + "_Othello.txt"
    partie.sauvegarder(filename)